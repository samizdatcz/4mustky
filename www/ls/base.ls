data = d3.tsv.parse ig.data.data
# data.length = 36
# data.reverse!
console.log d3.extent data.map ->
  parseInt it["skončil ve Světovém poháru"], 10
colors =
  "1":  \#a50026
  "2":  \#f46d43
  "3": \#fdae61
  "4": \#74add1
  "5": \#74add1
  "6": \#74add1
  "7": \#4575b4
  "8": \#4575b4
  "9": \#4575b4
  "10": \#313695
  "11": \#313695
toShortName = (name) ->
  [first, ...middle, last] = name.split " "
  "#{first[0]}. #last"

container = d3.select ig.containers.base
container.append \ul
  ..selectAll \li .data data .enter!append \li
    ..style \background-color -> colors[it["skončil ve Světovém poháru"]]
    ..append \span
      ..attr \class "note turne-note"
      ..html -> "vítěz Turné #{it.Sezona}"
    ..append \span
      ..attr \class \turne
      ..html -> toShortName it["Vítěz Turné"]
    ..append \span
      ..attr \class "note poradi-note"
      ..html "skončil v Poháru"
    ..append \span
      ..attr \class \poradi
      ..html -> it["skončil ve Světovém poháru"] + "."
    ..append \span
      ..attr \class \year
      ..html (.Sezona)
    ..filter(-> it["skončil ve Světovém poháru"] != "1")
      ..append \span
        ..attr \class "note svetovy-note"
        ..html "vyhrál"
      ..append \span
        ..attr \class \svetovy
        ..html -> toShortName it["Vítěz Světového poháru"]
